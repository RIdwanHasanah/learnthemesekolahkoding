<?php
//load scripts
function load_file(){
	wp_enqueue_style('style', get_stylesheet_uri() );
}

add_action('wp_enqueue_scripts','load_file');

/*===== the_excerpt =====*/
function get_excerpt_length(){  //untuk menampilkan jumlah kata artikel pada blog
	return 50;
}

add_filter('excerpt_length','get_excerpt_length' );

function return_excerpt_text(){ //untuk menampilkan lanjut baca pada artikel

	return '';

}

add_filter('excerpt_more', 'return_excerpt_text' );

/*===== Init Setup =====*/
function init_setup(){

	register_nav_menus(array(
		'main_menu'=>'Menu Utama',
		'footer_menu'=>'Menu Footer') );

	add_theme_support('post-thumbnails' ); //agar gambar  bisa tampil di post
	add_image_size('small_thumb',150,150,true ); //utk mengatur gambar thumbail
	add_image_size('big_thumb',500,500,true ); //utk mengatur gambar thumbail
	add_theme_support(
		'post-formats',
		array(
			'aside',
			'gallery',
			'video',
			'link',
			'quote',
			'status',
			'chat',
			'image',
			'audio')
		);

}

add_action('after_setup_theme','init_setup' );

/*==== Menambahkan widget ====*/
function widget_setup(){
	
	register_sidebar(array(
		'name'=>'Sidebar Pertama',
		'id'=>'sidebar1') );

	register_sidebar(array(
		'name'=>'Sidebar Kedua',
		'id'=>'sidebar2',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',) );
}

add_action('widgets_init', 'widget_setup');
?>