<?php get_header(); //menampilkan header ?>

<main>
	<?php
//mengecek jika ada post maka tampilkan
	if (have_posts() ) { 
	?>
		<p class="sub_judul">
			Hasil Pencarian ...
		</p>


	<?php
		while (have_posts() ) {
			if($post->post_type == 'page') continue;
		the_post(); //menampilkan post
		get_template_part('content'); //menambil dari file content.php
	}
}else{
	echo 'Nohing Post';
}

?>
</main>


<?php get_footer(); /*MEnampilkan footer*/ ?>