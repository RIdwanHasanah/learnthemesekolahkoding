<?php get_header(); //menampilkan header ?>

<main>
	<?php
//mengecek jika ada post maka tampilkan
	if (have_posts() ) {

		while (have_posts() ) {

		the_post(); //menampilkan post
		get_template_part('content',get_post_format() ); //menambil dari file content.php

	}
}else{
	echo 'Nohing Post';
}

?>
</main>
<aside>
	<?php dynamic_sidebar('sidebar1')?>
</aside>
<div class="clear"></div>

<?php get_footer(); /*MEnampilkan footer*/ ?>