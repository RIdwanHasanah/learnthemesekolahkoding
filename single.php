<?php get_header(); //menampilkan header ?>

<main>
<?php
//mengecek jika ada post maka tampilkan
if (have_posts() ) {

	while (have_posts() ) {

		the_post(); //menampilkan post
		get_template_part('content'); //menambil dari file content.php


		if ( comments_open() || get_comments_number() ) {
					comments_template();
		}
	}
}else{
	echo 'Nohing Post';
}

?>
</main>
<aside>
	<?php dynamic_sidebar('sidebar2')?>
</aside>
<div class="clear"></div>


<?php get_footer(); /*MEnampilkan footer*/ 

/*
-comments_open() Tag Bersyarat ini memeriksa apakah komentar diperbolehkan untuk Pos yang sekarang atau Post ID yang diberikan. Ini adalah fungsi boolean, artinya mengembalikan TRUE atau FALSE.

-get_comments_number() Ambil jumlah komentar yang dimiliki pos.

-comments_template() untuk menampikan template komentar
*/
?>
