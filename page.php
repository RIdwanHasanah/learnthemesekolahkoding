<?php get_header(); //menampilkan header ?>

<main id="main-page">
	<?php
//mengecek jika ada post maka tampilkan
	if (have_posts() ) {

		while (have_posts() ) {

		the_post(); //menampilkan post
		get_template_part('content'); //menambil dari file content.php
		
	}
}else{
	echo 'Nohing Post';
}

?>
</main>


<?php get_footer(); /*MEnampilkan footer*/ ?>