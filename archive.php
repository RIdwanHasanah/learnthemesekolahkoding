<?php get_header(); //menampilkan header ?>

<main>
	<?php
//mengecek jika ada post maka tampilkan
	if (have_posts() ) { ?>

	<p class="sub_judul">
	<?php
		if (is_category() ) {
			echo 'Halaman Kategori '; single_cat_title();
		}elseif (is_author()) {
			echo 'Halman Author ' . get_the_author();
		}else{
			echo 'Halaman Archive';
		}
	?>
	</p>
	<?php

	while (have_posts() ) {

		the_post(); //menampilkan post
		get_template_part('content');
	}
}else{
	echo 'Nohing Post';
}

?>
</main>


<?php get_footer(); /*MEnampilkan footer*/ ?>